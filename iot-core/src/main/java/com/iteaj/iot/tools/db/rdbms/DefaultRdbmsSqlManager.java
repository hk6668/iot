package com.iteaj.iot.tools.db.rdbms;

import com.iteaj.iot.FrameworkException;
import com.iteaj.iot.tools.db.DBManager;
import com.iteaj.iot.tools.db.DBMeta;
import com.iteaj.iot.tools.db.ParamValue;
import com.iteaj.iot.tools.db.RdbmsException;
import com.iteaj.iot.tools.db.sql.SqlStatementUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultRdbmsSqlManager implements DBManager<RdbmsHandle> {

    private DataSource dataSource;
    private Map<String, DBMeta> tableNameAndMetaMap = new HashMap<>(8);
    private Map<Class<?>, DBMeta> entityClassAndMetaMap = new HashMap<>(8);
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public DefaultRdbmsSqlManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public RdbmsMeta getDBMeta(String tableName) {
        return (RdbmsMeta) tableNameAndMetaMap.get(tableName);
    }

    public RdbmsMeta getDBMeta(Class entityClass) {
        DBMeta dbMeta = entityClassAndMetaMap.get(entityClass);
        if(dbMeta == null) {
            synchronized (this) {
                dbMeta = entityClassAndMetaMap.get(entityClass);
                if(dbMeta == null) {
                    dbMeta = new RdbmsMeta(entityClass);
                    entityClassAndMetaMap.put(entityClass, dbMeta);
                }
            }
        }

        return (RdbmsMeta) dbMeta;
    }

    @Override
    public RdbmsMeta remove(String tableName) {
        return (RdbmsMeta) tableNameAndMetaMap.get(tableName);
    }

    @Override
    public RdbmsMeta register(DBMeta meta) {
        if(meta instanceof RdbmsMeta) {
            return (RdbmsMeta) tableNameAndMetaMap.put(meta.getTableName(), meta);
        }

        throw new RdbmsException("只支持["+RdbmsMeta.class.getSimpleName()+"]类型对象");
    }

    @Override
    public RdbmsMeta registerIfAbsent(DBMeta meta) {
        if(!tableNameAndMetaMap.containsKey(meta.getTableName())) {
            return this.register(meta);
        }

        return null;
    }

    @Override
    public int insert(String tableName, Object entity) {
        RdbmsMeta dbMeta = this.getDBMeta(tableName);
        List<ParamValue> params = dbMeta.getParams(entity);
        return this.execUpdate(this.dataSource, dbMeta,1, params);
    }

    @Override
    public int insert(String tableName, Map<String, Object> value) {
        RdbmsMeta dbMeta = this.getDBMeta(tableName);
        List<ParamValue> params = dbMeta.getParams(value);
        return this.execUpdate(this.dataSource, dbMeta, 1, params);
    }

    @Override
    public int insert(Class entityClazz, Object entity) {
        RdbmsMeta dbMeta = this.getDBMeta(entityClazz);
        List<ParamValue> params = dbMeta.getParams(entity);
        return this.execUpdate(this.dataSource, dbMeta, 1, params);
    }

    @Override
    public int batchInsert(String tableName, List<Object> entities) {
        RdbmsMeta dbMeta = this.getDBMeta(tableName);
        List<ParamValue> values = new ArrayList<>();
        for (Object entity : entities) {
            List<ParamValue> params = dbMeta.getParams(entity);
            values.addAll(params);
        }

        return execUpdate(this.dataSource, dbMeta, entities.size(), values);
    }

    @Override
    public int batchInsert(Class entityClazz, List<Object> entities) {
        RdbmsMeta dbMeta = this.getDBMeta(entityClazz);
        List<ParamValue> values = new ArrayList<>();
        for (Object entity : entities) {
            List<ParamValue> params = dbMeta.getParams(entity);
            values.addAll(params);
        }

        return execUpdate(this.dataSource, dbMeta, entities.size(), values);
    }

    @Override
    public void execute(Object entity, RdbmsHandle handle) {
        RdbmsMeta dbMeta = this.getDBMeta(entity.getClass());
        List<ParamValue> params = dbMeta.getParams(entity);
        this.execUpdate(handle.rdbmsDataSource(entity), dbMeta, 1, params);
    }

    protected int execUpdate(DataSource dataSource, RdbmsMeta meta, int size, List<ParamValue> values) {
        long start = System.currentTimeMillis();
        String sqlStatement = meta.getStatement(size);
        try {
            try {
                Object[] params = values.stream().map(item -> item.getFieldValue()).toArray();
                Connection connection = dataSource.getConnection();
                return SqlStatementUtil.execute(connection, sqlStatement, params);
            } catch (SQLException e) {
                throw new FrameworkException(e);
            }
        } finally {
            if(logger.isTraceEnabled()) {
                StringBuilder sb = new StringBuilder();
                for (ParamValue value : values) {
                    sb.append(value.getFieldValue()).append(", ");
                }

                logger.trace("Rdbms适配 数据入库({}ms) - 条数：{}\r\n\t    Sql：{} \r\n\t params：{}"
                        , System.currentTimeMillis() - start, size, sqlStatement, sb.substring(0, sb.length() - 2));
            }
        }

    }
}
