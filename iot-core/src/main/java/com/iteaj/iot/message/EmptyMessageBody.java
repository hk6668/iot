package com.iteaj.iot.message;

/**
 * <p>空的报文体</p>
 * Create Date By 2017-09-12
 * @author iteaj
 * @since 1.7
 */
public class EmptyMessageBody extends DefaultMessageBody {

    private static EmptyMessageBody instance = new EmptyMessageBody();

    protected EmptyMessageBody() { }

    public static EmptyMessageBody getInstance() {
        return instance;
    }
}
