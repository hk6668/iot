package com.iteaj.iot;

import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.server.ServerComponent;

import java.util.List;

public interface ComponentFactory extends LifeCycle {

    int size();

    /**
     * 获取组件
     * @param port
     * @return
     */
    FrameworkComponent get(PortType type, Integer port);

    /**
     * 获取组件
     * @param messageClazz
     * @return
     */
    FrameworkComponent get(Class<? extends Message> messageClazz);

    /**
     * 客户端组件
     * @return
     */
    List<ClientComponent> clients();

    /**
     * 获取tcp协议的组件
     * @return
     */
    List<ServerComponent> servers();

    /**
     * 启用指定组件
     * @param messageClazz
     */
    void start(Class<?> messageClazz);

    /**
     * 停止组件, 不移除组件
     * @param messageClazz
     * @return
     */
    boolean stop(Class<?> messageClazz);

    /**
     * 关闭组件, 移除组件
     * @param messageClazz
     * @return
     */
    boolean close(Class<?> messageClazz);

    /**
     * 注册组件
     * @param component
     * @return
     */
    void register(FrameworkComponent component);
}
