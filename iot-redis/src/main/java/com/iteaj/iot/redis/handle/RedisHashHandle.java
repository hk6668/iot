package com.iteaj.iot.redis.handle;

import com.iteaj.iot.redis.consumer.HashConsumer;
import com.iteaj.iot.redis.producer.HashProducer;
import com.iteaj.iot.Protocol;

/**
 * 用来处理Redis Hash数据类型的生产和消费
 * @param <P>
 * @param <V>
 */
public interface RedisHashHandle<P extends Protocol, V> extends HashProducer<P, V>, HashConsumer<V> {

}
